# 언어 모델 아키텍처의 이해 <sup>[1](#footnote_1)</sup>

> <font size="3">신경망을 이용하여 언어 모델이 어떻게 설계되고 구현되는지 알아본다.</font>

## 목차

1. [들어가며](./language-model-architectures.md#intro)
1. [언어 모델이란?](./language-model-architectures.md#sec_02)
1. [언어 모델 타입](./language-model-architectures.md#sec_03)
1. [언어 모델링을 위한 신경망 아키텍처](./language-model-architectures.md#sec_04)
1. [언어 모델의 평가지표](./language-model-architectures.md#sec_05)
1. [과제와 향후 방향](./language-model-architectures.md#summary)

<a name="footnote_1">1</a>: [LLM Tutorial 2 — Understanding Language Model Architectures](https://medium.com/datadriveninvestor/llm-tutorial-2-understanding-language-model-architectures-e07d64d4f14b?sk=e566854de5c8ddc61cf38eca7dd6ffd3)를 편역한 것입니다.
